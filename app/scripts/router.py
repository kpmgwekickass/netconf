import re
import sys
import os

testCase = {}
blacklist = []
notApplicable = ["Banner Rules","Create an 'access-list' for use with SNMP","Set 'ip access-list extended' to Forbid Private Source Addresses from External Networks","Set inbound 'ip access-group' on the External Interface"]


def AAA():
        testCase["Enable 'aaa new-model'"] = re.search("aaa new-model",confRead)
        testCase["Enable 'aaa authentication-login'"] = re.search("aaa authentication login",confRead)
        testCase["Enable 'aaa authentication-enable default'"] = re.search("aaa authentication enable",confRead)
        testCase["Set 'login authentication for 'line con 0'"] = conditionalFind("line","con","login authentication")
        testCase["Set 'login authentication for 'line tty'"] = conditionalFind("line","tty","login authentication")
        testCase["Set 'login authentication for 'line vty'"] = conditionalFind("line","vty","login authentication")
        testCase["Set 'aaa accounting' to log all privileged use commands using 'commands 15'"] = re.search("aaa accounting commands 15",confRead)
        testCase["Set 'aaa accounting connection'"] = re.search("aaa accounting connection",confRead)
        testCase["Set 'aaa accounting exec'"] = re.search("aaa accounting exec",confRead)
        testCase["Set 'aaa accounting network'"] = re.search("aaa accounting network",confRead)
        testCase["Set 'aaa accounting system'"] = re.search("aaa accounting system",confRead)

def AccessRules():
        testCase["Set 'privilege 1' for local users"] = re.search("username [a-zA-z] password",confRead)
        testCase["Set 'transport input ssh' for 'line vty' connections"] = conditionalFind_number("line","vty","transport input ssh",number_vty)
        testCase["Set 'no exec' for 'line aux 0'"] = conditionalFind_number("line","aux","no exec",number_aux)
        testCase["Create 'access-list' for use with 'line vty'"] = re.findall("access-list [[0-9]|[1-5] permit]",confRead) 
        testCase["Set 'access-class' for 'line vty'"] = conditionalFind_number("line","vty","access-class",number_vty)
        testCase["Set 'exec-timeout' to less than or equal to 10 minutes for 'line aux 0'"] = conditionalFind_number("line","aux","exec-timeout [[0-9]|[0-5]]",number_aux) 
        testCase["Set 'exec-timeout' to less than or equal to 10 minutes 'line console 0'"]= conditionalFind_number("line","con","exec-timeout [[0-9]|[0-5]]",number_console) 
        testCase["Set 'exec-timeout' less than or equal to 10 minutes 'line tty'"] = conditionalFind_number("line","tty","exec-timeout [[0-9]|[0-5]]",number_vty) 
        testCase["Set 'exec-timeout' to less than or equal to 10 minutes 'line vty'"] = conditionalFind_number("line","vty","exec-timeout [[0-9]|[0-5]]",number_vty) 
        testCase["Set 'transport input none' for 'line aux 0'"] = conditionalFind_number("line","aux","transport input none",number_aux) 

def BannerRules():
        testCase["Banner Rules"] = "NA"

def PasswordRules():
        testCase["Set 'password' for 'enable secret'"] = re.search("enable secret [\w]+",confRead)
        testCase["Enable 'service password-encryption'"] = re.search("service password-encryption",confRead)
        testCase["Set 'username secret' for all local users"] = re.findall("username [\w]+ secret",confRead)

def SNMPRules():
        testCase["Set 'no snmp-server' to disable SNMP when unused"] = re.search("no snmp-server",confRead)
        testCase["Unset 'private' for 'snmp-server community'"] = re.search("no snmp-server community [\w]+",confRead)
        testCase["Unset 'public' for 'snmp-server community'"] = re.search("no snmp-server community [\w]+",confRead)
        testCase["Do not set 'RW' for any 'snmp-server community'"] = re.search("no snmp-server community [\w]+",confRead)
        testCase["Set the ACL for each 'snmp-server community'"] = re.search("snmp-server community [\w]+ ro [\w]+",confRead)
        testCase["Create an 'access-list' for use with SNMP"] = "NA"
        testCase["Set 'snmp-server host' when using SNMP"] = re.search("snmp-server host [0-9.] [\w]",confRead)
        testCase["Set 'snmp-server enable traps snmp'"] = re.search("snmp-server enable traps snmp authentication linkup linkdown coldstart",confRead)
        testCase["Set 'priv' for each 'snmp-server group' using SNMPv3"] = re.search("snmp-server group [\w]+ v3 priv",confRead)
        testCase["Require 'aes 128' as minimum for 'snmp-server user' when using SNMPv3"] = re.findall("snmp-server [\w]+ [\w]+ v3 encrypted auth sha [.] priv aes 128",confRead)


def GSR():
        testCase["Set the 'hostname'"] = re.search("hostname",confRead)
        testCase["Set the 'ip domain name'"] = re.search("ip domain name",confRead)
        testCase["Set 'modulus' to greater than or equal to 2048 for 'crypto key generate rsa'"] = re.search("crypto key generate rsa general-keys modulus 2048",confRead)
        testCase["Set 'seconds' for 'ip ssh timeout'"] = re.search("ip ssh time-out \d+",confRead)
        testCase["Set maximimum value for 'ip ssh authentication-retries'"]  = re.search("ip ssh authentication-retries \d+",confRead)
        testCase["Set version 2 for 'ip ssh version'"] = re.search("ip ssh version 2",confRead)
        testCase["Set 'no cdp run'"] = re.search("no cdp run",confRead)
        testCase["Set 'no ip bootp server'"] = re.search("no ip bootp sever",confRead)
        testCase["Set 'no service dhcp'"] = re.search("no service dhcp",confRead)
        testCase["Set 'no ip identd'"] = re.search("no ip identd",confRead)
        testCase["Set 'service tcp-keepalives-in'"] = re.search("service tcp-keepalives-in",confRead)
        testCase["Set 'service tcp-keepalives-out'"] = re.search("service tcp-keepalives-out",confRead)
        testCase["Set 'no service pad'"] = re.search("no service pad",confRead)

def LoggingRules():
        testCase["Set 'logging on'"] = re.search("logging on",confRead)
        testCase["Set 'buffer size' for 'logging buffered'"] = re.search("logging buffered [0-9]",confRead)
        testCase["Set 'logging console critical'"] = re.search("logging control critical",confRead)
        testCase["Set IP address for 'logging host'"] = re.search("logging host syslog_server",confRead)
        testCase["Set 'logging trap informational'"] = re.search("logging trap informational",confRead)
        testCase["Set 'service timestamps debug datetime'"] = re.search("service timestamps debug datetime [0-9]{1,2} show-timezone",confRead)
        testCase["Set 'logging source interface'"] = re.search("logging source-inerface loopback [.]",confRead)

def NTPRules():
        testCase["Set 'ntp authenticate'"] = re.search("ntp authenticate",confRead)
        testCase["Set 'ntp authentication-key'"] = re.search("ntp authentication-key [^\n] md5 [^\n]",confRead)
        testCase["Set the 'ntp trusted-key'"] = re.search("ntp trusted-key [^\n]",confRead)
        testCase["Set 'key' for each 'ntp server'"] = re.search("ntp server [^\n]",confRead)
        testCase["Set 'ip address' for 'ntp server'"] = re.search("ntp server [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}",confRead)

def LoopbackRules():
        testCase["Create a single 'interface loopback'"] = conditionalFind_number("interface","Loopback","ip address [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}",number_loop)
        testCase["Set AAA 'source-interface'"] = re.search("source-inerface loopback",confRead)
        testCase["Set 'ntp source' to Loopback Interface"] = re.search("ntp source Loopback[0-9]",confRead)
        testCase["Set 'ip tftp source-interface' to the Loopback Interface"] = re.search("ip tftp source-interface Loopback[0-9]",confRead)

def RoutingRules():
        testCase["Set 'no ip source-route'"] = re.search("no ip source-route",confRead)
        testCase["Set 'no ip proxy-arp'"] = conditionalFind_number("interface"," ","no ip proxy-arp",number_loop+number_ethernet+number_tunnel+number_vlan)
        testCase["Set 'no interface tunnel'"] = re.search("no interface tunnel",confRead)
        testCase["Set 'ip verify unicast source reachable-via'"] = conditionalFind_number("interface"," ","ip verify unicast source reachable-via rx",number_loop+number_ethernet+number_tunnel+number_vlan)

def BRF():
        testCase["Set 'ip access-list extended' to Forbid Private Source Addresses from External Networks"] = "NA"
        testCase["Set inbound 'ip access-group' on the External Interface"] = "Na"

def NA():
        testCase["Set 'key chain'"] = re.search("key chain [^\n]",confRead)
        testCase["Set 'key'"] = re.search("key [0-9]{1-3}",confRead)
        testCase["Set 'key-string'"] = re.search("key-string",confRead)
        if re.search("address-family ipv4",confRead):
                testCase["Set 'address-family ipv4 autonomous-system'"] = "True"
        else:
                testCase["Set 'address-family ipv4 autonomous-system'"] = conditionalFind_number("router","eigrp","address-family ipv4",1)

        testCase["Set 'authentication mode md5'"] = conditionalFind("router","eigrp","authentication mode md5")
        testCase["Set 'ip authentication key-chain eigrp'"] = conditionalFind("interface"," ","authentication key-chain eigrp")
        testCase["Set 'ip authentication mode eigrp'"] = conditionalFind("interface"," ","authentication mode eigrp")
        testCase["Set 'authentication message-digest' for OSPF area"] = conditionalFind("router","ospf","authentication message-digest")
        testCase["Set 'ip ospf message-digest-key md5'"] = conditionalFind("interface"," ","ip ospf message-digest-key")
        testCase["Set 'ip rip authentication key-chain'"] = conditionalFind("interface"," ","ip rip authentication key-chain")
        testCase["Set 'ip rip authentication mode' to 'md5'"] = conditionalFind("interface"," ","ip rip authentication mode md5")
        testCase["Set 'neighbor password'"] = conditionalFind("router","bgp","neighbour [^\n] password")

def NumberLineInterface():
        number = re.findall("interface FastEthernet",confRead)
        number_fast_ethernet = len(number)
        
        global number_ethernet
        global number_console
        global number_aux
        global number_vty
        global number_tty
        global number_loop
        global number_tunnel
        global number_vlan

        if number_fast_ethernet==0:
                number_ethernet = len(re.findall("interface GigabitEthernet",confRead)) - conditionalFind_count("interface","GigabitEthernet","shutdown")
        else:
                number_ethernet = len(re.findall("interface FastEthernet",confRead)) - conditionalFind_count("interface","FastEthernet","shutdown")

        number = re.findall("line con",confRead)
        number_console = len(number)

        number = re.findall("line aux",confRead)
        number_aux = len(number)

        number = re.findall("line vty",confRead)
        number_vty = len(number)

        number = re.findall("line tty",confRead)
        number_tty = len(number)

        number = re.findall("interface Loopback",confRead)
        number_loop = len(number)

        number = re.findall("interface Tunnel",confRead)
        number_tunnel = len(number)

        number = re.findall("interface vlan",confRead)
        number_vlan = len(number)


def conditionalFind(delimiter,key,parameter):
        confFiltered = confRead.split(delimiter)
        i = 0
        length = len(confFiltered)
        for i in range (0,length):
                if key in confFiltered[i]:
                        if parameter in confFiltered[i]:
                                flag = 1
                                break
                        else:
                                flag = 0
                else:
                        flag = 0
                        i = i+1
        return flag

def conditionalFind_number(delimiter,key,parameter,counter):
        confFiltered = confRead.split(delimiter)
        i = 0
        count = 1
        length = len(confFiltered)
        for i in range (0,length):
                if key in confFiltered[i]:
                        temp = re.search(parameter,confFiltered[i])
                        if temp:
                                count = count + 1
                else:
                        i = i+1

        if count==counter:
                flag = 1
        else:
                flag = 0
        return flag

def conditionalFind_count(delimiter,key,parameter):
        confFiltered = confRead.split(delimiter)
        i = 0
        count = 1
        length = len(confFiltered)
        for i in range (0,length):
                if key in confFiltered[i]:
                        temp = re.search(parameter,confFiltered[i])
                        if temp:
                                count = count + 1
                else:
                        i = i+1
        return count

def ManagementPlane():
        AAA()
        AccessRules()
        BannerRules()
        PasswordRules()
        SNMPRules()

def ControlPlane():
        GSR()
        LoggingRules()
        NTPRules()
        LoopbackRules()

def DataPlane():
        RoutingRules()
        BRF()
        NA()

def test():
        for case in testCase:
                if testCase[case]:
                        if case in blacklist:
                                testCase[case] = 'False'
                        else:                   
                                testCase[case] = 'True'
                else:
                        if case in blacklist:
                                testCase[case] = 'True'
                        elif case in notApplicable:
                                testCase[case] = "NA"
                        else:
                                testCase[case] = 'False'  

def main(filename):
        configFile = open("/home/sin9yt/netconf/app/scripts/uploads/" + filename)
        global confRead
        confRead = configFile.read()
        NumberLineInterface()
        ManagementPlane()
        ControlPlane()
        DataPlane()
        test()
        configFile.close()
        return testCase

if __name__ == '__main__':
        main()
