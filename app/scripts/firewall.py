import os
import sys
import re

testCase = {}
blacklist = ["Ensure 'Telnet' is disabled","Ensure DHCP services are disabled for untrusted interfaces","DHCPD","Ensure 'logging' is enabled","Ensure 'logging to Serial console' is disabled","Ensure 'ip verify' is set to 'reverse-path' for untrusted interfaces"]
notApplicable = ["Ensure 'RIP authentication' is enabled","Ensure 'OSPF authentication' is enabled","Ensure 'EIGRP authentication' is enabled"]

def passManagement():
	testCase["Ensure 'Logon Password' is set"] = re.search("passwd [\w.]+ encrypted",confRead)
	testCase["Ensure 'Enable Password' is set"] = re.search("enabled passwd [\w.]+ encrypted",confRead)

def deviceManagement():
	testCase["Ensure 'Domain Name' is set"] = re.search("domain-name \w+.[\w.]+",confRead)
	testCase["Ensure 'Host Name' is set"] = re.search("hostname [\w.]+",confRead)
	testCase["Ensure 'Failover' is enabled"] = re.search("^failover",confRead)

def AAA():
	testCase["Ensure 'aaa local authentication max failed attempts' is set to less than or equal to '3'"] = re.search("^aaa local authentication attempts max-fail",confRead)
	testCase["Ensure 'local username and password' is set"] = re.search("username \w+ password \w+ encrypted",confRead)
	testCase["Ensure 'TACACS+/RADIUS' is configured correctly"] = re.search("aaa-server [\w+]+ \(\w+\) host [\d.]+",confRead)
	testCase["Ensure 'aaa authentication enable console' is configured correctly"] = re.search("^aaa authentication enable",confRead)
	testCase["Ensure 'aaa authentication http console' is configured correctly"] = re.search("^aaa authentication http console",confRead)
	testCase["Ensure 'aaa authentication secure-http-client' is configured correctly"] = re.search("^aaa authentication secure-http-client",confRead)
	testCase["Ensure 'aaa authentication serial console' is configured correctly"] = re.search("aaa authentication serial console",confRead)
	testCase["Ensure 'aaa authentication ssh console' is configured correctly"] = re.search("aaa authentication ssh console",confRead)
	testCase["Ensure 'aaa authentication telnet console' is configured correctly"] = re.search("aaa authentication telnet console",confRead)
	testCase["Ensure 'aaa command authorization' is configured correctly"] = re.search("aaa authorization command",confRead)
	testCase["Ensure 'aaa authorization exec' is configured correctly"] = re.search("aaa authorization exec",confRead)
	testCase["Ensure 'aaa command accounting' is configured correctly"] = re.search("aaa accounting command",confRead)
	testCase["Ensure 'aaa accounting for SSH' is configured correctly"] = re.search("aaa accounting ssh console",confRead)
	testCase["Ensure 'aaa accounting for Serial console' is configured correctly"] = re.search("aaa accounting serial console",confRead)

def SSHRules():
	testCase["Ensure 'SSH source restriction' is set to an authorized IP address"] = re.search("ssh [\d.]+ [\d.]+ \w+",confRead)
	testCase["Ensure 'SSH version 2' is enabled"] = re.search("ssh version 2",confRead)
	testCase["Ensure 'SCP protocol' is set to Enable for files transfers"] = re.search("ssh scopy enable",confRead)
	testCase["Ensure 'Telnet' is disabled"] = re.search("^telnet",confRead) #OppValue

def HTTPRules():
	testCase["Ensure 'HTTP source restriction' is set to an authorized IP address"] = re.search("http [\d.]+ [\d.]+ \w+",confRead)
	testCase["Ensure 'TLS 1.0' is set for HTTPS access"] = re.search("ssl",confRead)

def sessTimout():
	testCase["Ensure 'console session timeout' is less than or equal to '5' minutes"] = re.search("console timeout \d+",confRead)
	testCase["Ensure 'SSH session timeout' is less than or equal to '5' minutes"] = re.search("ssh timeout \d+",confRead)
	testCase["Ensure 'HTTP session timeout' is less than or equal to '5' minutes"] = re.search("http server session-timeout \d+",confRead)
	
def ntpRules():
	testCase["Ensure 'NTP authentication' is enabled"] =re.search("ntp authenticate",confRead)
	testCase["Ensure 'NTP authentication key' is configured correctly"] = re.search("ntp authentication-key",confRead)
	testCase["Ensure 'trusted NTP server' exists"] = re.search("^ntp server",confRead)
	testCase["Ensure 'local timezone' is properly configured"] = re.search("clock timezone",confRead)

def loggingRules():
	testCase["Ensure 'logging' is enabled"] = re.search("logging enable",confRead)
	testCase["Ensure 'logging to Serial console' is disabled"] = re.search("logging console errors",confRead) #Opp	
	testCase["Ensure 'logging to monitor' is disabled"] = re.search("logging monitor debugging",confRead) #Opp
	testCase["Ensure 'syslog hosts' is configured correctly"] = re.search("logging host \w+ [\d.]+",confRead) 
	testCase["Ensure 'logging with the device ID' is configured correctly"] = re.search("logging device-id [\d\w.]+",confRead)
	testCase["Ensure 'logging history severity level' is set to greater than or equal to '5'"] = re.search("logging history",confRead)
	testCase["Ensure 'logging with timestamps' is enabled"] = re.search("logging timestamp",confRead)
	testCase["Ensure 'syslog logging facility' is equal to '23'"] = re.search("logging facility 23",confRead)
	testCase["Ensure 'logging buffer size' is greater than or equal to '524288'bytes (512kb)"] = re.search("logging buffer-size \d{6,}",confRead)
	testCase["Ensure 'logging buffered severity level' is greater than or equalto '3'"] = re.search("logging buffered (3|4|5|6|7)",confRead)
	testCase["Ensure 'logging trap severity level' is greater than or equal to '5'"] = re.search("logging trap (5|6|7)",confRead)
	testCase["Ensure email logging is configured for critical to emergency"] = re.search("logging mail critical",confRead)

def snmpRules():
	testCase["Ensure 'snmp-server group' is set to 'v3 priv'"] = re.search("snmp-server group [\D\w]+ v3 priv",confRead)
	testCase["Ensure 'snmp-server user' is set to 'v3 auth SHA'"] = re.search("snmp-server user [\D]+ v3 auth SHA",confRead)
	testCase["Ensure 'snmp-server host' is set to 'version 3'"] = re.search("snmp-server host \D+ [\d.]+ \D* version 3",confRead)
	testCase["Ensure 'SNMP traps' is enabled"] = re.search("snmp-server enable traps snmp authentication linkup linkdown coldstart",confRead)

def RPAuth():
	if re.search("router rip",confRead):
		testCase["Ensure 'RIP authentication' is enabled"] = re.search("rip authentication key") 
	if re.search("router ospf",confRead):
		testCase["Ensure 'OSPF authentication' is enabled"] = re.search("ospf message-digest-key \D+")
	if re.search("router eigrp",confRead):
		testCase["Ensure 'EIGRP authentication' is enabled"] = re.search("authentication key eigrp")
	testCase["Ensure 'noproxyarp' is enabled for untrusted interfaces"] = re.search("sysopt noproxyarp",confRead)
	testCase["Ensure 'DNS Guard' is enabled"] = re.search("dns-guard",confRead)
	
	testCase["Ensure DHCP services are disabled for untrusted interfaces"] = re.search("dhcpd enable",confRead) #Opp
	if not testCase["Ensure DHCP services are disabled for untrusted interfaces"]:
		testCase["DHCPD"] = re.search("dhcprelay enable",confRead) #Opp	
	else: testCase["DHCPD"] = "NA"
	testCase["Ensure ICMP is restricted for untrusted interfaces"] = re.search("icmp deny any",confRead)

def DataPlane():
	testCase["Ensure DNS services are configured correctly"] = re.search("dns name-server [\d.]+",confRead)
	testCase["Ensure intrusion prevention is enabled for untrusted interfaces"] = re.search("ip audit interface \D+",confRead)
	testCase["Ensure packet fragments are restricted for untrusted interfaces"] = re.search("fragment chain 1 \D+",confRead)
	testCase["Ensure non-default application inspection is configured correctly"] = re.search("inspect [\w\D]+",confRead)
	testCase["Ensure DOS protection is enabled for untrusted interfaces"] = re.search("set connection \D+ \d+",confRead)
	testCase["Ensure 'threat-detection statistics' is set to 'tcp-intercept'"] = re.search("threat-detection statistics tcp-intercept",confRead)
	testCase["Ensure 'ip verify' is set to 'reverse-path' for untrusted interfaces"] = re.search("ip verify reverse-path interface",confRead) #Opp
	testCase["Ensure 'security-level' is set to '0' for Internet-facing interface"] = re.search(" security-level \d+",confRead)
	testCase["Ensure Botnet protection is enabled for untrusted interfaces"] = re.search("dynamic-filter drop",confRead)
	testCase["Ensure ActiveX filtering is enabled"] = re.search("filter activex \d+ [\s\d.-]+",confRead)
	testCase["Ensure Java applet filtering is enabled"] = re.search("filter activex \d+ [\s\d.-]+",confRead)
	testCase["Ensure explicit deny in access lists is configured correctly"] = re.search("access-list \w+ extended deny [\w\s]+",confRead)


def test():
	for case in testCase:
		if testCase[case]:
			if case in blacklist:
				testCase[case] = 'False'
			else:			
				testCase[case] = 'True'
		else:
			if case in blacklist:
				testCase[case] = 'True'
			elif case in notApplicable:
				testCase[case] = "NA"
			else:
				testCase[case] = 'False'


def ManagementPlane():
	passManagement()
	deviceManagement()
	AAA()
	SSHRules()
	HTTPRules()
	sessTimout()
	ntpRules()
	loggingRules()
	snmpRules()
	
def ControlPlane():
	RPAuth()
	DataPlane()
	
def main(filename):
	configFile = open("/home/sin9yt/netconf/app/scripts/uploads/" + filename)
	global confRead
	confRead = configFile.read()
	ManagementPlane()
	ControlPlane()
	test()
	configFile.close()
	return testCase

if __name__ == '__main__':
	main()
