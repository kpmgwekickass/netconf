import os
import sys
import re

testCase = {}
blacklist = ['newmodel','hstname','loggon']

def AAArules():
	testCase['newmodel'] = re.search("no aaa new-model",confRead)#opp
	testCase['authlogin'] = re.search("aaa authentication login",confRead)
	testCase['authenbldef'] = re.search("aaa authentication enable default",confRead)
	testCase['authenblcon'] = re.search("line con 0",confRead)
	testCase['authenbltty'] = re.search("line tty",confRead)
	testCase['authenblvty'] = re.search("line vty",confRead)
	testCase['aaacount'] = re.search("aaa accounting commands 15",confRead)
	testCase['aaacountconn'] = re.search("aaa accounting connection default",confRead)
	testCase['aaacountexe'] = re.search("aaa accounting exec default",confRead)
	testCase['aaacountnet'] = re.search("aaa accounting network default",confRead)
	testCase['aaacountsys'] = re.search("aaa accounting system default",confRead)

def Accessrules():
	testCase['privl'] = re.search("privilege 1",confRead)
	testCase['trnsimpssh'] = re.search("transport input ssh",confRead)
	testCase['execlinaux'] = re.search("no exec",confRead)
	testCase['accesslist'] = re.search("access-list",confRead)
	testCase['accesslist1'] = re.search("deny any log",confRead)
	testCase['accesclas'] = re.search("access-class",confRead)#NC
	testCase['ecec-time'] = re.search("line con 0\n exec-timeout (0-9)$ ",confRead)
	testCase['transout'] = re.search("transport output none",confRead)

def Bannerules():
	testCase['bannerexec'] = re.search("banner exec",confRead)
	testCase['bannrlogin'] = re.search("banner login",confRead)
	testCase['bannrmot'] = re.search("banner motd",confRead)

	
def Passwdrules():
	testCase['passwd'] = re.search("enable secret \d+",confRead)
	testCase['passencr'] = re.search("service password-encryption",confRead)
	testCase['usrsecr'] = re.search("username .* secret .*",confRead)
	testCase['bannrlogin'] = re.search("banner login",confRead)
	testCase['bannrlogin'] = re.search("banner login",confRead)	
def snmpRules():
	testCase['snmpServGroup'] = re.search("snmp-server group [\D\w]+ v3 priv",confRead)
	testCase['snmpver'] = re.search("snmp-server host.*version 3",confRead)
	testCase['snmpServegrp'] = re.search("snmp-server group.*(auth|noauth|priv)",confRead)
	testCase['snmpTrap'] = re.search("snmp-server enable traps snmp authentication linkup linkdown coldstart",confRead)

def GlobalServicesrules():
	testCase['hstname'] = re.search("hostname Router",confRead)#opp
	testCase['domname'] = re.search("domain .*",confRead)
	testCase['rsakey'] = re.search("rsakeypair TP-self-signed",confRead)
	testCase['ipssh'] = re.search("ip ssh version 2",confRead)
	testCase['cdprun'] = re.search("no cdp enable",confRead)	
	testCase['ipbootpser'] = re.search("no ip .* server",confRead)	
	testCase['cdprun'] = re.search("no cdp enable",confRead)	
	testCase['itentd'] = re.search("no identd enable",confRead)	
	testCase['servcetcpi'] = re.search("service tcp-keepalives-in",confRead)	
	testCase['servcetcpo'] = re.search("service tcp-keepalives-out",confRead)	
	testCase['servpad'] = re.search("no service pad",confRead)	

def Loggingrules():
	testCase['loggon'] = re.search("logging on",confRead)#opp
	testCase['logbuff'] = re.search("logging buffered \d+",confRead)
	testCase['logcon'] = re.search("no logging console",confRead)
	testCase['logghost'] = re.search("logging host",confRead)
	testCase['servdebug'] = re.search("service timestamps debug datetime msec",confRead)	
	testCase['loggsrc'] = re.search("logging source-interface",confRead)
def NTPrules():
	testCase['ntpauth'] = re.search("ntp authenticate",confRead)
	testCase['logbuff'] = re.search("logging buffered \d+",confRead)
	testCase['logcon'] = re.search("no logging console",confRead)
	testCase['ntpauth'] = re.search("ntp authentication-key \d+",confRead)		
	testCase['ntpky'] = re.search("ntp trusted-key \d+",confRead)
	testCase['ntpserv'] = re.search("ntp server [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} key \d+",confRead)
	
def Loopbackrules():
	testCase['srcint'] = re.search("ip tacacs source-interface",confRead)
	testCase['ntpsrc'] = re.search("ntp source ",confRead)
	testCase['tftp src'] = re.search("logging source-interface",confRead)
	
def Routingrules():
	testCase['ipsrcroute'] = re.search("no ip source-route",confRead)
	testCase['iparp'] = re.search("no ip proxy-arp",confRead)
	testCase['intunl'] = re.search("interface tunnel",confRead)

def NeghbrAuth():
	testCase['keysec'] = re.search("key chain",confRead)
	testCase['keynum'] = re.search("key \d+",confRead)
	testCase['keystr'] = re.search("key-string",confRead)
	testCase['keysec'] = re.search("key chain",confRead)
	testCase['keynum'] = re.search("key \d+",confRead)
	testCase['addrfm4'] = re.search("address-family ipv4",confRead)
	testCase['addrfm6'] = re.search("address-family ipv6",confRead)
	testCase['authkey'] = re.search("authentication-key \d+ md5",confRead)
	testCase['authmesdig'] = re.search("authentication message-digest",confRead)
	testCase['mesdigkey'] = re.search("ospf message-digest-key [^\n] md5 ",confRead)
	testCase['autkeychn'] = re.search("authentication key-chain",confRead)

def test():
	for case in testCase:
		if testCase[case]:
			if case in blacklist:
				testCase[case] = 'False'
			else:			
				testCase[case] = 'True'
		else:
			if case in blacklist:
				testCase[case] = 'True'
			else:
				testCase[case] = 'False'		


def ManagementPln():
	AAArules()
	Accessrules()
	Bannerules()
	Passwdrules()
	snmpRules()	
	
	
def ControlPln():
	GlobalServicesrules()
	Loggingrules()
	NTPrules()
	Loopbackrules()
	
def DataPln():
	Routingrules()
	NeghbrAuth()

		
def main(filename):
	configFile = open("/home/sin9yt/netconf/app/scripts/uploads/" + filename)
	global confRead
	confRead = configFile.read()
	ManagementPln()
	ControlPln()
	DataPln()
	test()
	configFile.close()
	return testCase

if __name__ == '__main__':
	main()

