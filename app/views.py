import os,importlib,datetime
from flask import render_template,request,redirect,url_for,flash,session,abort
from werkzeug import secure_filename
from app import app
from scripts import firewall,switch,router
import json,re
import jinja2
import pdfkit
from io import FileIO as file
import pyPdf
from pyPdf import PdfFileWriter, PdfFileReader
from sqlalchemy.orm import sessionmaker
from tabledef import *
from flask import send_from_directory,make_response


engine = create_engine('sqlite:///netconf.db', echo=True)
ALLOWED_EXTENSIONS = set(['txt', 'doc'])

def allowed_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def findDevice(filename):
	fileRead = open(app.config['UPLOAD_FOLDER'] + filename)
	if re.search("ASA Version",fileRead.read()):
		return 2
	else: return 0

def gen_html(filenames,testCase,testName,testType,dateEnd,dateStart,elapse):
	fileOpen = open('/home/sin9yt/netconf/app/scripts/uploads/report.html')
	htmlFile = fileOpen.read()
	template = jinja2.Template(htmlFile)
	rendered = template.render(filenames=filenames,testCase=testCase,testName=testName,testType=testType,
								dateEnd=dateEnd,dateStart=dateStart,elapse=elapse)
	with open("/home/sin9yt/netconf/app/scripts/uploads/report-second.html", "w") as text_file:
		text_file.write("%s" % rendered)
	text_file.close()

def append_pdf(input,output):
    [output.addPage(input.getPage(page_num)) for page_num in range(input.numPages)]

@app.route('/')
@app.route('/login',methods=['GET','POST'])

def login():
	if request.method == 'POST':
 		POST_USERNAME = str(request.form['username'])
 		POST_PASSWORD = str(request.form['password'])
 		Session = sessionmaker(bind=engine)
 		s = Session()
 		query = s.query(User).filter(User.username.in_([POST_USERNAME]), User.password.in_([POST_PASSWORD]) )
 		result = query.first()
 		if result:
 		    session['logged_in'] = True
 		    return redirect(url_for('index'))
 		else:
 		    return render_template("login.html",
								title='Login',
								message=1)
    		
	
	if request.method == 'GET':
		if not session.get('logged_in'):
			return render_template("login.html",
							title='Login',
							message=0)
		else:		
			return redirect(url_for('index'))

@app.route('/logout')
def logout():
	session['logged_in'] = False
	return redirect(url_for('login'))

@app.route('/index')
def index():
	if not session.get('logged_in'):
		return redirect(url_for('login'))

	else:
		return render_template("index.html",
							title='Home')

@app.route('/dashboard',methods=['GET','POST'])
def dashboard():
	if request.method == 'POST':
		if not session.get('logged_in'):
			return redirect(url_for('login'))
		
		# check if the post request has the file part
		if 'config-file[]' not in request.files:
			return render_template("error.html",
								title='Error',
								message="No file.")
		testName = request.form['test-name']
		if not testName:
			return render_template("error.html",
								title='Error',
								message="Enter Client Name Plz..")
		uploaded_files = request.files.getlist('config-file[]')
		
		filenames = []
		testCase =[]
		testType =[]
		dateStart=[]
		dateEnd=[]
		elapse=[]
		device = ['Router/Switch','Switch','Firewall']

		for file in uploaded_files:
			if file.filename == '':
				return render_template("error.html",
										title='Error',
										message="For god sake, I take a file as input!")
			if file and allowed_file(file.filename):
				filename = secure_filename(file.filename)
				file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
				filenames.append(filename)

			else: return render_template("error.html",
										title='Error',
										message="Bad file format.")		
			
			deviceType = int(findDevice(filename))
			testType.append(device[deviceType])
			dtStart = datetime.datetime.now()
			dateStart.append(str(dtStart))																																																																																																																																																																																																																																																																																																																																																																																																																																																																											
			
			if deviceType == 0: testCase.append(router.main(filename))
			elif deviceType == 1: 	testCase.append(switch.main(filename))
			else:	testCase.append(firewall.main(filename))
			dtEnd = datetime.datetime.now()
			dateEnd.append(str(dtEnd))
			elapse.append(str(dtEnd-dtStart))

		gen_html(filenames,testCase,testName,testType,dateEnd,dateStart,elapse)
			
		return render_template("dashboard.html",
								title='Dashboard',
								filenames=filenames,
								testCase=testCase,
								testName=testName,
								testType=testType,
								dateEnd=dateEnd,
								dateStart=dateStart,elapse=elapse,noOfFiles=len(filenames))
	else:
		return render_template("error.html",
								title='Error',
								message="You're trying to access an invalid resource")

@app.route('/download')

def report():	
	if not session.get('logged_in'):
		return redirect(url_for('login'))

	else:
		
		try:
			statFile = open('/home/sin9yt/netconf/app/scripts/uploads/report-second.html')
		except IOError:
			return render_template("error.html",
									title='Error',
									message="The report does not exist! Please upload and try again.")
	
		pdfkit.from_file('/home/sin9yt/netconf/app/scripts/uploads/report-first.html', 'out1.pdf')
		pdfkit.from_file('/home/sin9yt/netconf/app/scripts/uploads/report-second.html', 'out2.pdf')
		
		statFile.close()

		output = PdfFileWriter()
		
		# Appending two pdf-pages from two different files
		append_pdf(PdfFileReader(open("out1.pdf","rb")),output)
		append_pdf(PdfFileReader(open("out2.pdf","rb")),output)
	
		# Writing all the collected pages to a file
		resultFile=open("/home/sin9yt/netconf/app/scripts/uploads/result.pdf","wb")
		output.write(resultFile)
		os.remove('out1.pdf')
		os.remove('out2.pdf')
		#os.remove('/home/sin9yt/netconf/app/scripts/uploads/report-second.html')
		resultFile.close()
		resp = make_response(send_from_directory('/home/sin9yt/netconf/app/scripts/uploads/', "result.pdf"))
		resp.headers['Cache-Control'] = 'no-store'
		return resp

@app.route('/scans')

def scan():
	return render_template("error.html",
								title='Error',
								message="Sorry, This page is under construction. Bear with us.")

@app.route('/policies')

def policies():
	if not session.get('logged_in'):
		return redirect(url_for('login'))
		
	return render_template("policies.html",
							title='Policies')