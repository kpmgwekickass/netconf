from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
 
engine = create_engine('sqlite:///netconf.db', echo=True)
Base = declarative_base()
 
########################################################################
class User(Base):
    """"""
    __tablename__ = "users"
 
    id = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)
 
    #----------------------------------------------------------------------
    def __init__(self, username, password):
        """"""
        self.username = username
        self.password = password
        
#########################################################################
class Result(Base):
    """"""
    __tablename__ = "scans"

    id = Column(Integer, primary_key=True)
    testName = Column(String)
    testCase = Column(String)
    testType = Column(String)
    noOfFiles = Column(String)
    dateStart = Column(String)
    dateEnd = Column(String)
    elapse = Column(String)


    #----------------------------------------------------------------------
    def __init__(self, testName, testCase,testType,noOfFiles,dateStart,
      dateEnd,elapse):

        self.username = username
        self.password = password
        self.testName = testName
        self.testCase= testCase
        self.testType= testType
        self.noOfFiles= noOfFiles
        self.dateStart= dateStart
        self.dateEnd= dateEnd
        self.elapse= elapse

# create tables
Base.metadata.create_all(engine)